package com.company;
import java.util.Scanner;

public class TicTacToe {
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";

	public static void thePictureOfGameField(String[][] gameField) {
		System.out.println(ANSI_PURPLE + "    1   2   3  " + ANSI_RESET);
		System.out.println(ANSI_CYAN + " ______________" + ANSI_RESET);
		System.out.println(ANSI_PURPLE + "1" + ANSI_RESET + ANSI_CYAN + " | " + ANSI_RESET + gameField[0][0] + ANSI_CYAN + " | " + ANSI_RESET + gameField[0][1] + ANSI_CYAN + " | " + ANSI_RESET + gameField[0][2] + ANSI_CYAN + " |" + ANSI_RESET);
		System.out.println(ANSI_CYAN + " --------------" + ANSI_RESET);
		System.out.println(ANSI_PURPLE + "2" + ANSI_RESET + ANSI_CYAN + " | " + ANSI_RESET + gameField[1][0] + ANSI_CYAN + " | " + ANSI_RESET + gameField[1][1] + ANSI_CYAN + " | " + ANSI_RESET + gameField[1][2] + ANSI_CYAN + " |" + ANSI_RESET);
		System.out.println(ANSI_CYAN + " --------------" + ANSI_RESET);
		System.out.println(ANSI_PURPLE + "3" + ANSI_RESET + ANSI_CYAN + " | " + ANSI_RESET + gameField[2][0] + ANSI_CYAN + " | " + ANSI_RESET + gameField[2][1] + ANSI_CYAN + " | " + ANSI_RESET + gameField[2][2] + ANSI_CYAN + " |" + ANSI_RESET);
		System.out.println(ANSI_CYAN + " --------------" + ANSI_RESET);
	}

	public static void inputPlayersValues(String[][] gameField, String symbol, Scanner scanner, String player) {
		while (true) {
			boolean isGame = winningLosingDrawState(gameField, player, symbol);
			if (!isGame)
				break;
			System.out.println("The player " + player + " insert the value");
			String playerEnter = scanner.nextLine();
			String[] playerPosition = playerEnter.split(" ");
			if (playerPosition.length == 1) {
				String str = playerPosition[0];
				playerPosition = new String[2];
				playerPosition[0] = str;
				playerPosition[1] = "0";
			}
			int parsedRowPosition = Integer.parseInt(playerPosition[0]);
			int parsedColumnPosition = Integer.parseInt(playerPosition[1]);
			if (parsedRowPosition - 1 > 2 || parsedColumnPosition - 1 > 2 || parsedRowPosition - 1 < 0 || parsedColumnPosition - 1 < 0) {
				System.out.println(ANSI_RED + "I don't have cell with such number, please enter other" + ANSI_RESET);
			} else {
				if (gameField[parsedRowPosition - 1][parsedColumnPosition - 1].equals(" ")) {
					gameField[parsedRowPosition - 1][parsedColumnPosition - 1] = symbol;
					thePictureOfGameField(gameField);
					break;
				} else {
					System.out.println(ANSI_RED + "This cell have already used, choose another cell" + ANSI_RESET);
				}
			}
		}
	}

	public static boolean winningLosingDrawState(String[][] gameField, String player, String symbol) {
		boolean isMethodWorks = true;
		if ((gameField[0][0] == symbol && gameField[0][1] == symbol && gameField[0][2] == symbol) || (gameField[1][0] == symbol && gameField[1][1] == symbol && gameField[1][2] == symbol) || (gameField[2][0] == symbol && gameField[2][1] == symbol && gameField[2][2] == symbol)) {
			System.out.println("The winner is " + ANSI_GREEN + player + ANSI_RESET);
			isMethodWorks = false;
			return isMethodWorks;
		}
		if ((gameField[0][0] == symbol && gameField[1][0] == symbol && gameField[2][0] == symbol) || (gameField[0][1] == symbol && gameField[1][1] == symbol && gameField[2][1] == symbol) || (gameField[0][2] == symbol && gameField[1][2] == symbol && gameField[2][2] == symbol)) {
			System.out.println("The winner is " + ANSI_GREEN + player + ANSI_RESET);
			isMethodWorks = false;
			return isMethodWorks;
		}
		if ((gameField[0][0] == symbol && gameField[1][1] == symbol && gameField[2][2] == symbol) || (gameField[0][2] == symbol && gameField[1][1] == symbol && gameField[2][0] == symbol)) {
			System.out.println("The winner is " + ANSI_GREEN + player + ANSI_RESET);
			isMethodWorks = false;
			return isMethodWorks;
		}
		if (gameField[0][0] != " " && gameField[0][1] != " " && gameField[0][2] != " " && gameField[1][0] != " " && gameField[1][1] != " " && gameField[1][2] != " " && gameField[2][0] != " " && gameField[2][1] != " " && gameField[2][2] != " ") {
			System.out.println(ANSI_PURPLE + "Draw" + ANSI_RESET);
			isMethodWorks = false;
			return isMethodWorks;
		}
		return isMethodWorks;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String cross = ANSI_YELLOW + "X" + ANSI_RESET;
		String zero = ANSI_BLUE + "O" + ANSI_RESET;
		System.out.println(ANSI_CYAN + "##### #  ####        #####   ##    ####        #####  ####  ###### \n" +
				"  #   # #    #         #    #  #  #    #         #   #    # #      \n" +
				"  #   # #      #####   #   #    # #      #####   #   #    # #####  \n" +
				"  #   # #              #   ###### #              #   #    # #      \n" +
				"  #   # #    #         #   #    # #    #         #   #    # #      \n" +
				"  #   #  ####          #   #    #  ####          #    ####  ######" + ANSI_RESET);
		System.out.println("Hey buddy, glad to see you there. Do you really want to play the game? You can use these com" +
				"mands: help,start,quit");
		while (true) {
			String firstUserCommand = scanner.nextLine();
			String[][] gameField = new String[3][3];
			boolean isGame = true;
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++)
					gameField[i][j] = " ";
			}
			switch (firstUserCommand) {
				case "quit":
					return;
				case "help":
					System.out.println("So, the rules of the game isn't that hard, but here is the gui" +
							"de how you can play in THIS thing:");
					System.out.println("1. To make a turn, you need to write the position in that form:number of row+spaceba" +
							"r+number of column.");
					System.out.println("For example: '1 1' it means that you place you symbol in cell that crosses first col" +
							"umn+row");
					System.out.println("2. Please, don't try to break the game. If you will do it, you will just have to rew" +
							"rite your value");
				case "start":
					System.out.println(ANSI_CYAN + " ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄               ▄  ▄▄▄▄▄▄▄▄▄▄▄       ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄        ▄ \n" +
							"▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌             ▐░▌▐░░░░░░░░░░░▌     ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░▌      ▐░▌\n" +
							"▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▐░▌           ▐░▌ ▐░█▀▀▀▀▀▀▀▀▀      ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌       ▐░▌▐░▌░▌     ▐░▌\n" +
							"▐░▌       ▐░▌▐░▌       ▐░▌  ▐░▌         ▐░▌  ▐░▌               ▐░▌          ▐░▌       ▐░▌▐░▌▐░▌    ▐░▌\n" +
							"▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌   ▐░▌       ▐░▌   ▐░█▄▄▄▄▄▄▄▄▄      ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌▐░▌ ▐░▌   ▐░▌\n" +
							"▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌    ▐░▌     ▐░▌    ▐░░░░░░░░░░░▌     ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌  ▐░▌  ▐░▌\n" +
							"▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌     ▐░▌   ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀      ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌       ▐░▌▐░▌   ▐░▌ ▐░▌\n" +
							"▐░▌       ▐░▌▐░▌       ▐░▌      ▐░▌ ▐░▌      ▐░▌               ▐░▌          ▐░▌       ▐░▌▐░▌    ▐░▌▐░▌\n" +
							"▐░▌       ▐░▌▐░▌       ▐░▌       ▐░▐░▌       ▐░█▄▄▄▄▄▄▄▄▄      ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░▌     ▐░▐░▌\n" +
							"▐░▌       ▐░▌▐░▌       ▐░▌        ▐░▌        ▐░░░░░░░░░░░▌     ▐░▌          ▐░░░░░░░░░░░▌▐░▌      ▐░░▌\n" +
							" ▀         ▀  ▀         ▀          ▀          ▀▀▀▀▀▀▀▀▀▀▀       ▀            ▀▀▀▀▀▀▀▀▀▀▀  ▀        ▀▀ \n" +
							"                                                                                                      " + ANSI_RESET);
					System.out.println("Please enter the name of " + ANSI_YELLOW + "first player" + ANSI_RESET + ", and after that enter the name of " + ANSI_BLUE + "second playe" +
							"r" + ANSI_RESET);
					System.out.println("The name of " + ANSI_YELLOW + "first player " + ANSI_RESET + "is: ");
					String theNameOfFirstPlayer = ANSI_YELLOW + scanner.nextLine() + ANSI_RESET;
					System.out.println("The name of " + ANSI_BLUE + "second player " + ANSI_RESET + "is: ");
					String theNameOfSecondPlayer = ANSI_BLUE + scanner.nextLine() + ANSI_RESET;
					System.out.println("So, you decide to play after reading help or just type by your fingers 'start' so he" +
							"re you can see our game field: ");
					thePictureOfGameField(gameField);
					while (isGame) {
						try {
							inputPlayersValues(gameField, cross, scanner, theNameOfFirstPlayer);
							isGame = winningLosingDrawState(gameField, theNameOfFirstPlayer, cross);
							if (!isGame)
								break;
							inputPlayersValues(gameField, zero,scanner, theNameOfSecondPlayer);
							isGame = winningLosingDrawState(gameField, theNameOfSecondPlayer, zero);
							if (!isGame)
								break;
						} catch (NumberFormatException e) {
							System.out.println(ANSI_RED + "This is not acceptable value, enter again" + ANSI_RESET);
						}
					}
					if (!isGame) {
						System.out.println("Do you want to try again? Y/N");
						boolean isPlayerWantToRerun = true;
						while (isPlayerWantToRerun) {
							String aftermathEnter = scanner.nextLine();
							switch (aftermathEnter) {
								case "N":
									System.out.println("Okay, see ya");
									return;
								case "Y":
									System.out.println("Okay, let's try again");
									isPlayerWantToRerun = false;
									break;
								default:
									System.out.println("Please, just type Y or N");
									break;
							}
						}
					}
					break;
				default:
					System.out.println(ANSI_YELLOW + "Lol, man, what's wrong with you, restart the program" + ANSI_RESET);
					break;
			}
		}
	}
}
